# Contributor: Andrej Kolchin <KAAtheWise@protonmail.com>
# Contributor: fossdd <fossdd@pwned.life>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=vale
pkgver=3.9.2
pkgrel=0
pkgdesc="A markup-aware linter for prose built with speed and extensibility in mind"
url="https://vale.sh/"
arch="all"
license="MIT"
makedepends="go"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/errata-ai/vale/archive/v$pkgver.tar.gz"
options="net"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -ldflags "-X main.version=v$pkgver" -o bin/vale ./cmd/vale
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/vale -t "$pkgdir"/usr/bin
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
73cb1cd63a96e5b873ce3e2ab7efd6924e4a61c9e39d0e13d41fed3161b0de2a52a7a4a6064aec1ed3573c16c76bec1d11e2e3e696ecc74c35e8a4bd9d05493e  vale-3.9.2.tar.gz
"
